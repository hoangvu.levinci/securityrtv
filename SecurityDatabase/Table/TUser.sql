﻿CREATE TABLE [dbo].[TUser]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [FullName] VARCHAR(64) NOT NULL, 
    [Username] VARCHAR(32) NOT NULL, 
    [Password] VARCHAR(32) NOT NULL, 
    [RoleId] INT NOT NULL,
    [Status] INT NOT NULL, 
)
