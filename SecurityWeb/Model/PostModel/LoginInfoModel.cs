﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SecurityWeb.Model.PostModel
{
    public class LoginInfoModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}