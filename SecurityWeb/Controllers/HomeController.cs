﻿using SecurityModel.Model;
using SecurityWeb.Model.PostModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SecurityWeb.Controllers
{
    public class HomeController : Controller
    {
        [FilterLogin]
        public ActionResult Index()
        {
            using (var db = new SecurityDatabaseEntities())
            {
                var users = db.TUser.ToList();

                ViewBag.Users = users;
            }
            return View();
        }
    }
}