﻿using SecurityModel.Model;
using SecurityService.Implement;
using SecurityService.Interface;
using SecurityWeb.Model.PostModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SecurityWeb.Controllers
{
    public class AuthController : Controller
    {
        // GET: Login
        private readonly IAuthService _authService;

        public AuthController()
        {
            _authService = new AuthService();
        }
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult LoginApi(LoginInfoModel model)
        { 
            var users = _authService.GetUser(model.Username,model.Password);
            if (users == null)
                return RedirectToAction("Index","Auth");
            else
            {
                Session["Username"] = users.Username;
                return RedirectToAction("Index","Home");
            }
        }

    }
}