﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SecurityWeb.Controllers
{
    public class FilterLogin : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string username = (string)(HttpContext.Current.Session["Username"]);

            if (username == null)
            {
                filterContext.Result = new RedirectResult("/Auth/Index");
                return;
               // return RedirectToAction("Index", "Auth");
            }
        }
    }
}