﻿using SecurityModel.Model;
using SecurityService.Interface;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecurityService.Implement
{
    public class EntityService<TEntity> : IEntityService<TEntity> where TEntity : class
    {
        protected readonly SecurityDatabaseEntities _context;
        private readonly DbSet<TEntity> _dbSet;

        public EntityService(IDbContext context)
        {
            _context = context.GetContext();
            _dbSet = _context.Set<TEntity>();
        }

        public TEntity Get(int id)
        {
            return _dbSet.Find(id);
        }

        public IQueryable<TEntity> GetAll()
        {
            return _dbSet.AsQueryable();
        }
    }
}
