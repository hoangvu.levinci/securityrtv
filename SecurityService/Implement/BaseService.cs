﻿using SecurityModel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecurityService.Implement
{
    public interface IDbContext : IDisposable
    {
        SecurityDatabaseEntities GetContext();
    }

    public class BaseService
    {
        protected readonly SecurityDatabaseEntities _context;

        public BaseService(IDbContext context)
        {
            _context = context.GetContext();

        }
    }

    public class SecurityDbContext : IDbContext
    {
        private SecurityDatabaseEntities _context;

        public SecurityDbContext()
        {
            _context = new SecurityDatabaseEntities();
            _context.Configuration.LazyLoadingEnabled = false;
        }

        public SecurityDatabaseEntities GetContext()
        {
            return _context;
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
