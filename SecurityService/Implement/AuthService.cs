﻿using SecurityModel.Model;
using SecurityService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecurityService.Implement
{
    public class AuthService : EntityService<TUser>, IAuthService
    {
        public AuthService(IDbContext context) : base(context)
        {
        }

        public TUser GetUser(string username, string password)
        {
            return _context.TUser.FirstOrDefault(u => u.Username == username && u.Password == password);
        }
    }
}
