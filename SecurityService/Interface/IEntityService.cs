﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecurityService.Interface
{
    public interface IEntityService<TEntity>
    {
        TEntity Get(int id);
        IQueryable<TEntity> GetAll();
    }
}
