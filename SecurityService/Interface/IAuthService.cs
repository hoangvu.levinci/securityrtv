﻿using SecurityModel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecurityService.Interface
{
    public interface IAuthService : IEntityService<TUser>
    {
        TUser GetUser(string username, string password);

    }
}
