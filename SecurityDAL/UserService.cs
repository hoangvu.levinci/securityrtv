﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecurityDAL
{
    public class UserService : BaseService
    {
        public Login GetUserByUsernamePass(LoginInfoModel mode)
        {
            return GetContext().Login.FirstOrDefault(u => u.UserName == mode.Username && u.Password == mode.Password);
        }
    }
}
